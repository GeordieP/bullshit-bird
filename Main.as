package {
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.getTimer;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Timer;

	public class Main extends MovieClip {
		// Container for everything - to deal with layering and GUI
		var StageContainer:MovieClip;

		// Timing
		var fakeDeltaTime:Number = 1; 			// for the hell of it!
		var ObstacleSpawnTimer:Timer;

		// Obstacles
		var obstaclesContainer:ObstaclesContainer;
		var obstacleArray:Array;

		// Player
		var player:Player;

		// Physics
		var GRAVITY_CONSTANT:Number = 9;		// arbitrary value
		var appliedForce:Point;

		// Gameplay
		var HUD:HudDisplay;
		var score:int;
		var highestScore:int;

		public function Main():void {
			this.gotoAndStop(1);	// go to pre-game screen
			StageContainer = new MovieClip();
			addChild(StageContainer);

			// Only set this once at the very beginning of the game
			highestScore = 0;

			// add the HUD after the stage container so it's drawn on top
			HUD = new HudDisplay();
			HUD.x = stage.stageWidth / 2;

			PlayBtn.addEventListener(MouseEvent.MOUSE_UP, BtnClick);
		}

		public function BtnClick(e:MouseEvent):void {
			switch (e.target.name) {
				case "PlayBtn":
					StartGame();
				break;
				case "ResetBtn":
					StartGame();
				break;
			}
		}

		public function StartGame():void {
			// remove event listeners on buttons
			if (PlayBtn) PlayBtn.removeEventListener(MouseEvent.MOUSE_DOWN, BtnClick);
			if (ResetBtn) ResetBtn.removeEventListener(MouseEvent.MOUSE_DOWN, BtnClick);

			ResetGame();

			addChild(HUD);

			// Timing
			stage.addEventListener(Event.ENTER_FRAME, Update);
			ObstacleSpawnTimer = new Timer(1000, 0);
			ObstacleSpawnTimer.start();

			stage.addEventListener(MouseEvent.MOUSE_DOWN, MouseDown);
			ObstacleSpawnTimer.addEventListener(TimerEvent.TIMER, SpawnObstacle);

			this.gotoAndStop(2);
		}

		public function ResetGame():void {
			// Obstacles
			obstaclesContainer = new ObstaclesContainer();
			StageContainer.addChild(obstaclesContainer);
			obstacleArray = new Array();

			// Gameplay
			SetScore(0);

			// Player
			player = new Player(new Point(220, 135), GRAVITY_CONSTANT);
			StageContainer.addChild(player);
		}

		public function StopGame():void {
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, MouseDown);
			ObstacleSpawnTimer.removeEventListener(TimerEvent.TIMER, SpawnObstacle);
			stage.removeEventListener(Event.ENTER_FRAME, Update);
		}

		public function Update(e:Event):void {
			var playerRectangle = new Rectangle(player.x, player.y, player.width, player.height);

			for (var i:int = 0; i < obstacleArray.length; i++) {
				var obst:Obstacle = obstacleArray[i];
				var obstacleRectangle = new Rectangle(obst.x, obst.y, obst.width, obst.height);

				if (obst.canCollide && playerRectangle.intersects(obstacleRectangle)) {
					var colType:int = obst.SubCollision(playerRectangle);
					if (colType == 1) {
						GameOver();
						obst.canCollide = false;
					} else if (colType == 2) {
						SetScore(score + 1);
						obst.canCollide = false;
					}
				}

				if (obst.IsOffScreen()) {		// if the obstacle has passed the left side of the screen
					DeleteObstacle(i);
				} else {
					obst.Update();
				}
			}

			if (player.y > stage.stageHeight || (player.y + player.height) < 0) {
				GameOver();
			}

			player.Update(fakeDeltaTime);
		}

		public function MouseDown(e:MouseEvent):void {
			if (player.alive) player.Bounce();
		}

		private function DeleteObstacle(index:int):void {
			obstacleArray.splice(index, 1);
		}

		private function GameOver():void {
			this.gotoAndStop(3);
			EndGameScoreDisplay.text = "" + score;

			if (score > highestScore) highestScore = score;
			EndGameHighScoreDisplay.text = "" + highestScore;

			StopGame();
			removeChild(HUD);
			StageContainer.removeChild(player);
			obstaclesContainer.removeChildren();
			ResetBtn.addEventListener(MouseEvent.MOUSE_UP, BtnClick);
		}

		public function SpawnObstacle(e:TimerEvent):void {
			var tempObst:Obstacle = new Obstacle(852, 480);
			obstaclesContainer.addChild(tempObst);
			obstacleArray.push(tempObst);
		}

		public function SetScore(score):void {
			this.score = score;
			HUD.ScoreDisplay.text = "" + score;
		}
	}
}
