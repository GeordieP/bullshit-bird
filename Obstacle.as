package {
	import flash.display.MovieClip;
	import flash.geom.Rectangle;

	public class Obstacle extends MovieClip {
	/*
		private var MIN_GAP_SIZE:int = 100;
		private var MAX_GAP_SIZE:int = 328;		// maximum gap size while keeping the pipes at their minimum height

		private var MIN_PIPE_HEIGHT:int;
		private var MAX_PIPE_HEIGHT:int;

		private var OBSTACLE_WIDTH = 100;
		private var MOVE_SPEED:int = 5;
		private var gapSize:int;
		public var canCollide:Boolean = true;

		var topSection:ObstacleBlock;
		var bottomSection:ObstacleBlock;

		public function Obstacle(screenWidth:int, screenHeight:int):void {
			MIN_PIPE_HEIGHT = 76;			// Height of a pipe
			MAX_PIPE_HEIGHT = screenHeight - MIN_GAP_SIZE - (MIN_PIPE_HEIGHT * 2);		// minPipeSize * 2 is to account for the minimum sizes of the pipes - need to be able to at least show both of them 
			gapSize = (Math.floor(Math.random() * (MAX_GAP_SIZE - MIN_GAP_SIZE + 1)) + MIN_GAP_SIZE);

			// based on the gap size, what is the minimum and maximum pipe size we can have?
				// then generate a number based on that, use it for the first, set the second as the remaining space

			var maxPipeHeight = screenHeight - gapSize;
			
			var firstHeight:int = (Math.floor(Math.random() * (maxPipeHeight - MIN_PIPE_HEIGHT + 1)) + MIN_PIPE_HEIGHT);	// this is a random portion of the remaining screen space
			var secondHeight:int = maxPipeHeight - firstHeight;


			// trace("Max Pipe Height: " + maxPipeHeight + " | Gap Size: " + gapSize + " | TEMP: " + temp + " | FirstHeight: " + firstHeight + " | SecondHeight " + secondHeight);
			topSection = new ObstacleBlock(0, 0, firstHeight, OBSTACLE_WIDTH, 0);
			bottomSection = new ObstacleBlock(0, screenHeight - secondHeight, secondHeight, OBSTACLE_WIDTH, 1);


			this.x = screenWidth + OBSTACLE_WIDTH;

			this.addChild(topSection);
			this.addChild(bottomSection);
		}*/



		/* Old system; works for now */
		private var MINIMUM_GAP_SIZE:int = 120; 		// the smallest a hole can be - adjust to change one aspect of difficulty
		private var MAXIMUM_OBSTACLE_HEIGHT:int = 340;
		private var MINIMUM_OBSTACLE_HEIGHT:int = 10;
		private var OBSTACLE_WIDTH = 100;
		private var MOVE_SPEED:int = 5;
		public var canCollide:Boolean = true;

		var topSection:ObstacleBlock;
		var bottomSection:ObstacleBlock;

		public function Obstacle(screenWidth:int, screenHeight:int):void {
			var firstHeight:int = Math.floor((Math.random() * MAXIMUM_OBSTACLE_HEIGHT) + MINIMUM_OBSTACLE_HEIGHT);		// height of the first obstacle created (located at the top of the screen)
			var secondHeight:int = screenHeight - MINIMUM_GAP_SIZE - firstHeight;

			// obstacle width isn't really the same as the movieclip it's supposed to be for..
			// topSection = new ObstacleBlock(-(OBSTACLE_WIDTH / 2))
			topSection = new ObstacleBlock(0, 0, firstHeight, OBSTACLE_WIDTH, 0);
			bottomSection = new ObstacleBlock(0, screenHeight - secondHeight, secondHeight, OBSTACLE_WIDTH, 1);

			this.x = screenWidth + OBSTACLE_WIDTH;

			this.addChild(topSection);
			this.addChild(bottomSection);
		}

		public function Update():void {
			this.x -= MOVE_SPEED;
		}

		public function IsOffScreen():Boolean {
			if (this.x + this.width < 0) return true;
			return false;
		}

		public function SubCollision(playerRectangle:Rectangle):int {
			// Add this.x to each x to convert from local coords to global
			var topSectionRectangle = new Rectangle(topSection.x + this.x, topSection.y, topSection.width, topSection.height);
			var bottomSectionRectangle = new Rectangle(bottomSection.x + this.x, bottomSection.y, bottomSection.width, bottomSection.height);

			if (playerRectangle.intersects(topSectionRectangle) || playerRectangle.intersects(bottomSectionRectangle)) {
				return 1;		// Collided with a block
			} else if (playerRectangle.x >= (this.x + (this.width / 2))) {
				return 2;		// Score a point
			}

			return 0;			// not an important collision
		}
	}
}
