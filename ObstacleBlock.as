package {
	import flash.display.MovieClip;

	public class ObstacleBlock extends MovieClip {
		public function ObstacleBlock(x:int, y:int, height:int, width:int, topOrBottom:int):void {
			this.x = x;
			this.y = y;
			this.height = height;
			this.width = width;

			if (topOrBottom == 0) {		// top
				this.gotoAndStop(2);
			} else {
				this.gotoAndStop(1);
			}
		}
	}
}
