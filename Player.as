package {
	import flash.display.MovieClip;
	import flash.geom.Point;
	import flash.geom.Rectangle;


	public class Player extends MovieClip {

		private var velocity:Point;
		private var acceleration:Point;
		public var appliedForce:Point;
		private var mass:Number;
		private var GRAVITY_CONSTANT:Number;
		public var alive:Boolean = true;

		public function Player(_position:Point, _GRAVITY_CONSTANT:Number):void {
			this.x = _position.x;
			this.y = _position.y;
			mass = 17;					// arbitrary value

			GRAVITY_CONSTANT = _GRAVITY_CONSTANT;

			velocity = new Point(0, 0);
			acceleration = new Point(0, 0);
			appliedForce = new Point(0, GRAVITY_CONSTANT);
		}

		public function Update(fakeDeltaTime:Number):void {
			// physics class LIED TO ME!
			acceleration.x = appliedForce.x / mass;
			acceleration.y = appliedForce.y / mass;

			velocity.x += acceleration.x * fakeDeltaTime;
			velocity.y += acceleration.y * fakeDeltaTime;

			this.x += velocity.x * fakeDeltaTime + (acceleration.x / 2) * (fakeDeltaTime * fakeDeltaTime);
			this.y += velocity.y * fakeDeltaTime + (acceleration.y / 2) * (fakeDeltaTime * fakeDeltaTime);
		}

		public function Bounce():void {
			acceleration.y = 0;
			velocity.y = -GRAVITY_CONSTANT;
		}

		public function Dead():void {
			alive = false;
		}
	}
}
